#################Dockerfile##################################
# in the local machine
FROM python:3.7
RUN mkdir -p /home/ubuntu/lemon/
WORKDIR  /home/ubuntu/lemon/
ADD . /home/ubuntu/lemon/
ENV HOME=/home/ubuntu
RUN pip3 install -r /home/ubuntu/lemon/requirements.txt


####################### bash script #################################
#!/bin/bash
# in the local machine
aws ecr get-login-password --region ca-central-1 | docker login --username AWS --password-stdin   ****.amazon.com
sudo docker build -t lemon .
sudo docker tag lemon:latest   ****.amazon.com/lemon_rep:latest
sudo docker push  ****.amazon.com/lemonlevel_rep:latest
echo "image upload to ECR"

#########################bash script ########################################33
# bash script  for pulling the code from the ecr 
# this bash script will be in the aws ec2 instance
# will setup the cronjob for this bash script to run
aws ecr get-login-password --regin ca-central-1 | docker login --username AWS -password-stdin  ****.amazon.com
sudo docker pull  ****.amazon.com/lemon_rep
docker-compose stop
docker-compose up

########################docker-compose file#####################################33
version: '3.7'

services:
  web:
    image: 090663405287.dkr.ecr.c
    command: python manage.py runserver 0.0.0.0:8000
    ports:
      - 8000:8000

#####################################################################
will setup the nginx as a proxy to forward the 443 and 80 requests
